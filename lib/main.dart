import 'package:flutter/material.dart';
import 'package:poc_qr_scanner_service/mobile_scanner_example/mobile_scanner_example.dart';
import 'package:poc_qr_scanner_service/qr_code_scanner_example/qr_code_scanner_example.dart';
import 'package:poc_qr_scanner_service/qr_mobile_vision_example/qr_mobile_vision_example.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'POC QR Scanner Service',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('POC QR Scanner Service'),
      ),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _getMobileScannerNavigationButton(),
            const SizedBox(height: 20),
            _getQrCodeScannerNavigationButton(),
            const SizedBox(height: 20),
            _getQrMobileVisionNavigationButton(),
          ],
        ),
      ),
    );
  }

  /*
  *Recommended*
  Package Name: mobile_scanner
  Package Version Used: v3.0.0
  Package URL: https://pub.dev/packages/mobile_scanner
  Independent Release APK Size: 17 MB

  Description:
  A universal barcode and QR code scanner for Flutter based on MLKit.
  Uses CameraX on Android, AVFoundation on iOS and Apple Vision & AVFoundation on macOS.
   */
  Widget _getMobileScannerNavigationButton() {
    return ElevatedButton(
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => const MobileScannerExample(),
          ),
        );
      },
      child: const Text('MobileScanner Example'),
    );
  }

  /*
  Package Name: qr_code_scanner
  Package Version Used: v1.0.1
  Package URL: https://pub.dev/packages/qr_code_scanner
  Independent Release APK Size: 7.5 MB

  Description:
  QR code scanner that can be embedded inside flutter.
  It uses zxing in Android and MTBBarcode scanner in iOS.

  Warning:
  Since the underlying frameworks of this package, zxing for android and MTBBarcodescanner
  for iOS are both not longer maintained, this plugin is no longer up to date and in maintenance mode only.
   */
  Widget _getQrCodeScannerNavigationButton() {
    return ElevatedButton(
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => const QrCodeScannerExample(),
          ),
        );
      },
      child: const Text('Qr Code Scanner Example'),
    );
  }

  /*
  Package Name: qr_mobile_vision
  Package Version Used: v4.0.1
  Package URL: https://pub.dev/packages/qr_mobile_vision
  Independent Release APK Size:  15 MB

  Description:
  Plugin for reading QR codes using Firebase's Mobile Vision API.

  Warning:
  The library hasn't been updated from 6 months and there
  were some internal SDK issues seen. Also it requires Firebase Setup
  everytime.
   */
  Widget _getQrMobileVisionNavigationButton() {
    return ElevatedButton(
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => const QrMobileVisionExample(),
          ),
        );
      },
      child: const Text('Qr Mobile Vision Example'),
    );
  }
}
